//
//  User.swift
//  Firebase_TodoList
//
//  Created by Slava Starovoitov on 06.11.2021.
//

import Foundation
import Firebase
import FirebaseAuth

struct UserModel {
    let uid: String
    let email: String
    
    init(user: User) {
        self.uid = user.uid
        self.email = user.email!
    }
}
