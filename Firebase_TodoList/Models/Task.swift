//
//  Task.swift
//  Firebase_TodoList
//
//  Created by Slava Starovoitov on 06.11.2021.
//

import Foundation
import Firebase
import FirebaseDatabase

struct TaskModel {
    let title: String
    let userId: String
    let ref: DatabaseReference?
    var completed: Bool = false
    
    init(title: String, userId: String) {
        self.title = title
        self.userId = userId
        self.ref = nil
    }
    
    init(snapshot: DataSnapshot) {
        let snapshotValue = snapshot.value as! [String: Any]
        title = snapshotValue["title"] as? String ?? ""
        userId = snapshotValue["user"] as? String ?? ""
        completed = snapshotValue["completed"] as! Bool
        ref = snapshot.ref
    }
    
    func convertToDictionary() -> Any {
        return ["title": title, "userId": userId, "completed": completed]
    }
}
